﻿


namespace Homework_6
{
    using System;
    using System.Collections.Generic;

    using System.Globalization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

        public partial class DictionaryItem
        {
            [JsonProperty("word")]
            public string Word { get; set; }

            [JsonProperty("definitions")]
            public Definition[] Definitions { get; set; }

            [JsonProperty("pronunciation")]
            public string Pronunciation { get; set; }
        }

        public partial class Definition
        {
            [JsonProperty("type")]
            public string Type { get; set; }

            [JsonProperty("definition")]
            public string DefinitionDefinition { get; set; }

            [JsonProperty("image_url")]
            public Uri ImageUrl { get; set; }

            [JsonProperty("emoji")]
            public object Emoji { get; set; }

            [JsonProperty("example")]
            public string Example { get; set; }
        }

        public partial class DictionaryItem
        {
            public static DictionaryItem FromJson(string json) => JsonConvert.DeserializeObject<DictionaryItem>(json, Homework_6.Converter.Settings);
        }

        public static class Serialize
        {
            public static string ToJson(this DictionaryItem self) => JsonConvert.SerializeObject(self, Homework_6.Converter.Settings);
        }

        internal static class Converter
        {
            public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
            {
                MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
                DateParseHandling = DateParseHandling.None,
                Converters =
            {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
            };
        }
    
}