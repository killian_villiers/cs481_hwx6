﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Homework_6;
using Plugin.Connectivity;

namespace Homework_6
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            
        }

        async void ButtonLauncher_ClickedAsync(System.Object sender, System.EventArgs e)
        {
            if (!CrossConnectivity.Current.IsConnected)
            {
                //You are offline, notify the user
                await DisplayAlert("Alert", "No Internet", "OK");
            }
            else
            {
                if (string.IsNullOrEmpty(EntryDic.Text))
                {
                    await DisplayAlert("Alert", "Empty Word", "OK");

                }

                else
                {
                    // You are online
                    //create new client
                    HttpClient client = new HttpClient();

                    //create new uri to use owlbot
                    var uri = new Uri(
                        string.Format($"https://owlbot.info/api/v4/dictionary/" + EntryDic.Text));
                    //create a new request http
                    var request = new HttpRequestMessage();
                    request.Method = HttpMethod.Get;
                    request.RequestUri = uri;

                    request.Headers.Authorization = new AuthenticationHeaderValue("Token", "9bb4af82c6b48d75e8b1634552486150b8a4faa0");

                    //send request
                    HttpResponseMessage response = await client.SendAsync(request);
                    DictionaryItem dictionaryData = null;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = await response.Content.ReadAsStringAsync();
                        dictionaryData = DictionaryItem.FromJson(content);
                        WordL.Text = WordL.Text + dictionaryData.Word;
                        TypeL.Text = TypeL.Text + dictionaryData.Definitions[0].Type;
                        DefinitionL.Text = DefinitionL.Text + dictionaryData.Definitions[0].DefinitionDefinition;
                        PronuncationL.Text = PronuncationL.Text + dictionaryData.Pronunciation;
                        ExampleL.Text = ExampleL.Text + dictionaryData.Definitions[0].Example;
                        EmojiL.Text = EmojiL.Text + dictionaryData.Definitions[0].Emoji;
                        ImageL.Source = dictionaryData.Definitions[0].ImageUrl;

                    }
                    else
                    {
                        await DisplayAlert("Alert", "Unknown word", "OK");
                    }
                }
                
            }
            


        }
    }
}
